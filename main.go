// +build linux

package main

import (
	"flag"
	"log"

	"bitbucket.org/m2dot/fswatcher/inotify"
)

func main() {
	path := flag.String("path", ".", "the directory to watch for events")
	flag.Parse()

	observer, err := NewObserver(*path)
	if err != nil {
		log.Fatalf("could not initialize observer: %v", err)
	}

	events, err := inotify.WatchDirectory(*path)
	if err != nil {
		log.Fatalf("could not setup watcher: %v", err)
	}

	for event := range events {
		err := observer.CheckOwner(event)
		if err != nil {
			log.Printf("check failed: %v", err)
		}
	}
}
