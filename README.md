# Filesystem Watcher

this is a simple daemon logging changes in permissions for files in
the /var/opt/tableau/tableau_server/data/tabsvc/services/ directory.
Those changes can stop Tableau backgrounder processes from working
correctly.

## Install

Download and install the .deb package and run
```bash
sudo systemctl enable fswatcher.service
sudo systemctl start fswatcher.service
```
to activate it.

## Usage

The information on permission changes can be seen with
```bash
journalctl -u fswatcher.service
```
