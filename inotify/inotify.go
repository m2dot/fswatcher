// +build linux

package inotify

import (
	"bytes"
	"fmt"
	"log"
	"path/filepath"
	"syscall"
	"unsafe"
)

// WatchDirectory registers the given path with inotify.
// it returns a channel on which, every time the permissions
// on the directory or it's direct children change, the name
// of the file or directory that changed is sent.
func WatchDirectory(path string) (<-chan string, error) {
	fd, err := syscall.InotifyInit()
	if err != nil {
		return nil, fmt.Errorf("could not initialize inotify: %w", err)
	}

	_, err = syscall.InotifyAddWatch(fd, path, syscall.IN_ATTRIB)
	if err != nil {
		return nil, fmt.Errorf("could not register %q in inotify: %w", path, err)
	}

	events := make(chan string, 1)
	go readEvents(fd, path, events)

	return events, nil
}

// runs in its own goroutine
func readEvents(fd int, path string, events chan string) {
	defer syscall.Close(fd)

	var buf [(syscall.SizeofInotifyEvent + syscall.NAME_MAX + 1) * 20]byte
	for {
		offset := 0
		n, err := syscall.Read(fd, buf[:])
		if err != nil {
			log.Printf("could not read from inotify file descriptor: %v", err)
			continue
		}

		for offset < n {
			event := (*syscall.InotifyEvent)(unsafe.Pointer(&buf[offset]))
			namebuf := buf[offset+syscall.SizeofInotifyEvent : offset+syscall.SizeofInotifyEvent+int(event.Len)]
			name := string(bytes.TrimRight(namebuf, "\x00"))
			events <- filepath.Join(path, name)
			offset += int(syscall.SizeofInotifyEvent + event.Len)
		}
	}
}
