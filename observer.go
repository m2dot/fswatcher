package main

import (
	"fmt"
	"log"
	"os"
	"os/user"
	"path/filepath"
	"strconv"
	"strings"
	"syscall"
)

type Observer struct {
	stats map[string]os.FileInfo
}

func NewObserver(path string) (*Observer, error) {
	rootStat, err := os.Stat(path)
	if err != nil {
		return nil, fmt.Errorf("could not read stats for %q: %w", path, err)
	}
	if !rootStat.IsDir() {
		return nil, fmt.Errorf("the supplied path is not a directory!")
	}

	stats := make(map[string]os.FileInfo)
	stats[path] = rootStat

	childs, err := os.ReadDir(path)
	if err != nil {
		return nil, fmt.Errorf("could not read directory entries for %q: %w", path, err)
	}

	for _, child := range childs {
		childPath := filepath.Join(path, child.Name())
		childStat, err := child.Info()
		if err != nil {
			return nil, fmt.Errorf("could not read stats for %q: %w", childPath, err)
		}
		stats[childPath] = childStat
	}

	return &Observer{stats}, nil
}

func (o *Observer) CheckOwner(path string) error {
	newStats, err := os.Stat(path)
	if err != nil {
		return fmt.Errorf("could not read stats for %q: %w", path, err)
	}
	newOwner, err := getOwnership(newStats)
	if err != nil {
		return fmt.Errorf("could not get Ownership info for %q: %w", path, err)
	}
	oldStats := o.stats[path]
	if oldStats == nil {
		log.Printf("new entry %q with ownership: %s", path, newOwner)
		o.stats[path] = newStats
		return nil
	}
	oldOwner, err := getOwnership(oldStats)
	if err != nil {
		return fmt.Errorf("could not get Ownership info for %q: %w", path, err)
	}
	if strings.Compare(oldOwner, newOwner) != 0 {
		log.Printf("ownership of %q changed from %s to %s", path, oldOwner, newOwner)
		o.stats[path] = newStats
	}
	return nil
}

func getOwnership(stats os.FileInfo) (string, error) {
	info, ok := stats.Sys().(*syscall.Stat_t)
	if !ok {
		return "", fmt.Errorf("internal error!")
	}
	uid := strconv.FormatUint(uint64(info.Uid), 10)
	gid := strconv.FormatUint(uint64(info.Gid), 10)
	u, err := user.LookupId(uid)
	if err != nil {
		return "", fmt.Errorf("could not find user with uid %v: %w", uid, err)
	}
	g, err := user.LookupGroupId(gid)
	if err != nil {
		return "", fmt.Errorf("could not find group with gid %v: %w", gid, err)
	}
	return fmt.Sprintf("%s:%s", u.Username, g.Name), nil
}
